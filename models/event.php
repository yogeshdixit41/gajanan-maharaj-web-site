<?php


/**
 * Description of Event
 *  This class represents an event object 
 *
 * @author Swaroop
 */
class Event {
    /** title of the event */
    private $title; 
    /** description of the event */
    private $description ;
    /** date of the event */
    private $date;
    
    /** 
     * @return string title of the event 
     */
    public function getTitle() {
        return $this->title;
    }

    /** 
     * sets title of the event 
     * 
     * @param title - title to be set
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /** 
     * @return string description of the event 
     */
    public function getDescription() {
        return $this->description;
    }

    /** 
     * sets Description of the event 
     * 
     * @param description - description to be set
     */
    public function setDescription($description) {
        $this->description = $description;
    }
    
    /** 
     * @return string date of the event 
     */
    public function getDate() {
        return $this->date;
    }
    
    /** 
     * sets Date of the event 
     * 
     * @param date - date to be set
     */
    public function setDate($date) {
        $this->date = $date;
    }
}