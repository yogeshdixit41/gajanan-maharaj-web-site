<?php

include('models/event.php');
   
class XmlParser {
    const ROOT_TAG = 'dataroot' ;
    /** event element name in events.xml */
    const EVENT_TAG = 'event' ;
    /** title element name in events.xml */
    const TITLE_TAG = 'title' ;
    /** description element name in events.xml */
    const DESCRIPTION_TAG = 'description' ;
    /** date element name in events.xml */
    const DATE_TAG = 'date' ;
    /** event element attribute */
    const EVENT_ID_ATTRIBUTE = 'eventId' ;
    /** file name constant events.xml */
    const EVENTS_XML_PATH = 'resources/events.xml' ;
    /** all events collection */
    private $allEvents = array() ;
        
    /** 
    * @return array of all events in xml
    */
    public function getAllEvents() {
        return $this->allEvents ;
    }
    
    /**
     * this function parse xml file into event objects and stores into an array
     */
    public function parseEvents() {
        // creates a new dom parser
        $xmlDoc = new DOMDocument();
        // load the xml to be parsed to get events
        $xmlDoc->load(self::EVENTS_XML_PATH) ;
        //console.log('Parsing started..') ;
        // get events list from the xml
        $events = $xmlDoc->getElementsByTagName(self::EVENT_TAG) ;
        // total event count
        $eventCount = 0 ;
        // store each event in array
        foreach ($events as $event) {
            // get new event object to store event values
            $eventObject = new Event();
            // get title, description and date values for event 
            foreach ($event->childNodes as $childNode) {
                switch ($childNode->nodeName) {
                    // get title of the event
                    case self::TITLE_TAG:
                        $eventObject->setTitle ($childNode->nodeValue) ;
                        break;
                    // get description of the event
                    case self::DESCRIPTION_TAG:
                        $eventObject->setDescription($childNode->nodeValue) ;
                        break ;
                    // get date of the event
                    case self::DATE_TAG:
                        $eventObject->setDate($childNode->nodeValue) ;
                        break ;
                }
            }
            // store event object in all events array
            $this->allEvents[$eventCount++] = $eventObject ;
        }
        //console.log('Parsing finished..');
    }
    
    public function addEvent($eventId,$eventName,$eventDate,$eventDescription) {
        // create dom document
        $xmlDoc = new DOMDocument() ;
        // load events xml file
        $xmlDoc->load(self::EVENTS_XML_PATH) ;
        // fetch root element
        $rootElement = $xmlDoc->getElementsByTagName(self::ROOT_TAG)->item(0);
        // create event element to add in events
        $event = $xmlDoc->createElement(self::EVENT_TAG) ;
        // create event's date element
        $dateElement = $xmlDoc->createElement(self::DATE_TAG,$eventDate) ;
        // create event's title element
        $titleElement = $xmlDoc->createElement(self::TITLE_TAG,$eventName) ;
        // create event's description element
        $descriptionElement = $xmlDoc->createElement(self::DESCRIPTION_TAG,$eventDescription) ;
        // append title element to event element
        $event->appendChild($titleElement) ;
        // append date element to event element
        $event->appendChild($dateElement) ;
        // append description element to event element
        $event->appendChild($descriptionElement) ;
        // append event element to document
        $appendResult = $rootElement->appendChild($event) ;
        // set eventid attribute to new event
        $appendResult->setAttribute(self::EVENT_ID_ATTRIBUTE, $eventId) ;
        // save modified event xml
        $xmlDoc->save(self::EVENTS_XML_PATH) ;
    }
}


