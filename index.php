<!DOCTYPE html>
<html lang="en">
<head>
    <title>&#2360;&#2375;&#2357;&#2366;&#2343;&#2366;&#2352;&#2368;&#2360;&#2340;&#2381;&#2360;&#2306;&#2327;&#2344;&#2381;&#2351;&#2366;&#2360;</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.png">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="all">
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:400,500,700' rel='stylesheet' type='text/css'>
    <script src="js/jquery-1.8.0.min.js"></script>
    <!--[if lt IE 9]><script src="js/modernizr.custom.js"></script><![endif]-->
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="js/functions.js"></script>
</head>

<body>
    <div id="wrapper">
    <!-- top-nav -->
        <nav class="top-nav">
            <div class="shell"> <a href="#" class="nav-btn">HOMEPAGE<span></span></a> <span class="top-nav-shadow"></span>
                <ul>
                    <li class="active"><span><a href="index.php">&#2346;&#2361;&#2367;&#2354;&#2375; &#2346;&#2366;&#2344;</a></span></li>
                    <li><span><a href="#">&#2360;&#2306;&#2346;&#2352;&#2381;&#2325;</a></span></li>
                </ul>
            </div>
            <span><a id="signin" href="loginPage.php">Sign In</a></span>
        </nav>

        <!-- end of top-nav -->
        <!-- header -->
        <header  id="header">
        <!-- shell -->
            <div class="shell">
                <div class="header-inner">
                <!-- header-cnt -->
                    <div class="header-cnt">
                        <h1 id="logo"><a href="index.php">Simple</a></h1>
                                            </div>
                    <!-- end of header-cnt -->
                    <!-- slider -->
                    <div class="slider-holder">
                        <div class="flexslider">
                            <ul class="slides">
                                <li><img src="css/images/gm1.png" alt=""></li>
                                <li><img src="css/images/gm2.png" alt=""></li>
                                <li><img src="css/images/gm3.png" alt=""></li>
                            </ul>
                        </div>
                    </div>
                    <!-- end of slider -->
                    <div class="cl">&nbsp;</div>
                </div>
                <div class="cl">&nbsp;</div>
            </div>
            <!-- end of shell -->
        </header>
        <!-- end of header -->
        <!-- main -->
        <div class="main"> <span class="shadow-top"></span>
        <!-- shell -->
            <div class="shell1">
                <div class="container">
                <!-- testimonial -->
                    <section class="testimonial">
                    <?php
                        include('xml/xmlParser.php');
                        $xmlData = new XmlParser() ;
                        // get all events from the xml
                        $xmlData->parseEvents() ;
                        $allEvents = $xmlData->getAllEvents() ;   
						
						$total_size = sizeof($allEvents);
						
						for($i = $total_size-1; $i>=0 ; $i--)
						{
							echo "<h1>".$allEvents[$i]->getTitle()."</h1>" ;
							echo '<p><strong>'.$allEvents[$i]->getDescription().'</strong></p>' ;
							echo "-------------------------------------------------------------";
						}
						/*foreach ($allEvents as $arr) {
							echo "<h1>".$arr->getTitle()."</h1>" ;
							echo '<p><strong>'.$arr->getDescription().'</strong></p>' ;
							echo "-------------------------------------------------------------";
							
						}*/
                        /* @var currentEvent represents current event to show */
                        //$currentEvent = $allEvents[1] ;
                        //echo "<h1>".$currentEvent->getTitle()."</h1>" ;
                        //echo '<p><strong>'.$currentEvent->getDescription().'</strong></p>' ;
                    ?>
                    </section>
                    <!-- testimonial -->
				<!--
                    <div class="footer-bottom">
                        <div class="shell">
                            <p class="copy">&copy; Copyright 2012<span>|</span>Sitename. Design by iDispatch Tech.</p>
                        </div>
                    </div>
				-->
                </div>
                <!-- end of footer -->
            </div>
        </div>
    </div>
</body>
</html>