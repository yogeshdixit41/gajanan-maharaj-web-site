<!DOCTYPE html>
<html lang="en">
<head>
<title>Simple</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0">
<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="all">
<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,500,700' rel='stylesheet' type='text/css'>
<script src="js/jquery-1.8.0.min.js"></script>
<!--[if lt IE 9]><script src="js/modernizr.custom.js"></script><![endif]-->
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/functions.js"></script>
<script type="text/javascript">
	function stopRKey(evt)
	{
		var evt= (evt) ? evt :  ((event) ? event : null) ;
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null) ;
		if((evt.keyCode == 13) && (node.type=="text")) {
			return false ;
		} 
		
	}
</script>
</head>

<body>
    <div id="wrapper">
    <!-- top-nav -->
    <nav class="top-nav">
        <div class="shell"> <a href="#" class="nav-btn">HOMEPAGE<span></span></a> <span class="top-nav-shadow"></span>
            <ul>
                <li><span><a href="index.php">home</a></span></li>
                <li  class="active"><span><a href="#">Contact Us</a></span></li>
            </ul>
        </div>
    </nav>
    <!-- end of top-nav -->
    <!-- header -->
    <header  id="header">
    <!-- shell -->
        <div class="shell">
            <div class="header-inner">
            <!-- header-cnt -->
                <div class="header-cnt">
                    <h1 id="logo"><a href="#">Simple</a></h1>
                    <p> <span class="mobile"></span> <span class="desktop"></span> </p>
                </div>
            <!-- end of header-cnt -->
            <!-- slider -->
            <div class="slider-holder">
                <div class="flexslider">
                    <ul class="slides">
                        <li><img src="css/images/gm1.png" alt=""></li>
                        <li><img src="css/images/gm2.png" alt=""></li>
                        <li><img src="css/images/gm3.png" alt=""></li>
                    </ul>
                </div>
            </div>
            <!-- end of slider -->
                <div class="cl">&nbsp;</div>
            </div>
            <div class="cl">&nbsp;</div>
        </div>
        <!-- end of shell -->
    </header>
    <!-- end of header -->
    <!-- main -->
    <div class="main"> <span class="shadow-top"></span>
    <!-- shell -->
        <div class="shell">
            <div class="container">
            <!-- testimonial -->
            <section class="testimonial">
<!-- login table -->
<form action="eventAdded.php" method="POST">
		
<table class="informationTable" border=0>
<tr>
	<td>
		<label class="formLables">Title</label></td><td><input class="formInput" type="text" name="title">
	</td>
</tr>
<tr>
	<td>
		<label class="formLables">Date</label></td><td><input type="date" name="date">
	</td>
</tr>

<tr>
	<td>
		<label class="formLables">Description</label>
	</td>
	<td>
		<textarea class="formTextArea" name="desc"></textarea>
	</td>
</tr>

<tr>
	<td>
	</td>
	<td>
		<input type="submit" style="height: 30px; width: 54px;float: right" value="submit">
		<input type="reset" style="height: 30px; width: 54px;float: right" value="reset">
	</td>
</tr>

</table>


</form>
            </section>
        <!-- testimonial -->
  
        <div class="footer-bottom">
            <div class="shell">
                <nav class="footer-nav">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Contacts</a></li>
                    </ul>
                    <div class="cl">&nbsp;</div>
                </nav>
                <p class="copy">&copy; Copyright 2012<span>|</span>Sitename. Design by iDispatch Tech.</a></p>
            </div>
        </div>
    </div>
    <!-- end of footer -->
 </body>
</html>
