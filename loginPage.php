<!DOCTYPE html>
<html lang="en">
<head>
<title>&#2360;&#2375;&#2357;&#2366;&#2343;&#2366;&#2352;&#2368;&#2360;&#2340;&#2381;&#2360;&#2306;&#2327;&#2344;&#2381;&#2351;&#2366;&#2360;</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0">
<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.png">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="all">
<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,500,700' rel='stylesheet' type='text/css'>
<script src="js/jquery-1.8.0.min.js"></script>
<!--[if lt IE 9]><script src="js/modernizr.custom.js"></script><![endif]-->
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/functions.js"></script>
<script type="text/javascript">
	function stopRKey(evt)
	{
		var evt= (evt) ? evt :  ((event) ? event : null) ;
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null) ;
		if((evt.keyCode == 13) && (node.type=="text")) {
			return false ;
		} 
		
	}
</script>
</head>

<body>
    <div id="wrapper">
    <!-- top-nav -->
    <nav class="top-nav">
        <div class="shell"> <a href="#" class="nav-btn">HOMEPAGE<span></span></a> <span class="top-nav-shadow"></span>
            <ul>
                <li><span><a href="index.php">&#2346;&#2361;&#2367;&#2354;&#2375; &#2346;&#2366;&#2344;</a></span></li>
                <li  class="active"><span><a href="#">&#2360;&#2306;&#2346;&#2352;&#2381;&#2325;</a></span></li>
            </ul>
        </div>
    </nav>
    <!-- end of top-nav -->
    <!-- header -->
    <header  id="header">
    <!-- shell -->
        <div class="shell">
            <div class="header-inner">
            <!-- header-cnt -->
                <div class="header-cnt">
                    <h1 id="logo"><a href="#">Simple</a></h1>
                    <p> <span class="mobile"></span> <span class="desktop"> .</span> </p>
                </div>
            <!-- end of header-cnt -->
            <!-- slider -->
            <div class="slider-holder">
                <div class="flexslider">
                    <ul class="slides">
                        <li><img src="css/images/gm1.png" alt=""></li>
                        <li><img src="css/images/gm2.png" alt=""></li>
                        <li><img src="css/images/gm3.png" alt=""></li>
                    </ul>
                </div>
            </div>
            <!-- end of slider -->
                <div class="cl">&nbsp;</div>
            </div>
            <div class="cl">&nbsp;</div>
        </div>
        <!-- end of shell -->
    </header>
    <!-- end of header -->
    <!-- main -->
    <div class="main"> <span class="shadow-top"></span>
    <!-- shell -->
        <div class="shell">
            <div class="container">
            <!-- testimonial -->
            <section class="testimonial">
<!-- login table -->

	<ul id="login-freebie">
		<li>
				<form method="post" action="Login_Authentication.php">
				<div class="login-freebie">
					<div class="form">
						<input type="text" id="username" name="username" placeholder="username"/>
						<input type="password" id="username" name="password" placeholder="password"/>
					</div>
					<div class="footer">
						
						<input type="reset" id="remember" value="reset"  /><label  for="remember"></label>
						
						<input type="submit" id="remember1" value="submit" /><label for="remember"></label>

					</div>
				</div>
				</form>
		</li>
	</ul>




            </section>
        <!-- testimonial -->
  
        <div class="footer-bottom">
            <div class="shell">
                <nav class="footer-nav">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Contacts</a></li>
                    </ul>
                    <div class="cl">&nbsp;</div>
                </nav>
                <p class="copy">&copy; Copyright 2012<span>|</span>Sitename. Design by iDispatch Tech.</a></p>
            </div>
        </div>
    </div>
    <!-- end of footer -->
 </body>
</html>

